#!/bin/bash
set -eu # Abort the script if a command returns with a non-zero exit code or if
        # a variable name is dereferenced when the variable hasn't been set
RED='\033[0;31m'
GREEN='\033[0;32m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Color

printf "\n${PURPLE}###### Setup the example\n${NC}"
groupadd ncsadmin
groupadd ncsoper
useradd --create-home --home-dir /home/admin --no-user-group --no-log-init --groups ncsadmin --shell /bin/bash admin
useradd --create-home --home-dir /home/oper --no-user-group --no-log-init --groups ncsoper --shell /bin/bash oper
echo "oper:oper" | chpasswd

python -m pip install --root-user-action=ignore --upgrade pip
python -m pip install --root-user-action=ignore requests
yum install -y openssh-clients

printf "${GREEN}##### Regen the NSO SSH server host key\n${NC}"
rm -f ${NCS_CONFIG_DIR}/ssh/*key*
ssh-keygen -N "" -t ed25519 -f ${NCS_CONFIG_DIR}/ssh/ssh_host_ed25519_key

printf "${GREEN}##### Add the RESTCONF token authentication script\n${NC}"
cp /${NSOAPP_NAME}/token_auth.sh ${NCS_RUN_DIR}/scripts/

printf "${GREEN}##### Reload packages\n${NC}"
ncs_cmd -d -c 'maction "/packages/reload"'

printf "${GREEN}##### Edit the init files\n${NC}"
sed -i.orig -e "s|_EX0_IP_|${EX0_IP}|" \
            -e "s|_EX1_IP_|${EX1_IP}|" \
            -e "s|_EX2_IP_|${EX2_IP}|" \
            /${NSOAPP_NAME}/devices_init.xml

printf "${GREEN}##### Load init files\n${NC}"
for f in /${NSOAPP_NAME}/*_init.xml
do
    ncs_load -d -m -l $f
done

printf "${GREEN}##### Edit ncs.conf\n${NC}"
sed -i.orig -e 's|</cli>|\ \ <style>c</style>\
\ \ </cli>|' \
            -e 's|</restconf>|\ \ <token-response>\
\ \ \ \ \ \ <x-auth-token>true</x-auth-token>\
\ \ \ \ </token-response>\
\ \ </restconf>|' \
            -e "s|</webui>|\ \ <server-name>${NSO_HOST_NAME}</server-name>\n\
\ \ \ \ <match-host-name>true</match-host-name>\n\
\ \ </webui>|" \
            -e "s|</aaa>|\ \ <external-validation>\n\
\ \ \ \ \ \ <enabled>true</enabled>\n\
\ \ \ \ \ \ <executable>${NCS_RUN_DIR}/scripts/token_auth.sh</executable>\n\
\ \ \ \ </external-validation>\n\
\ \ </aaa>|" \
            -e 's|@ncs|@nso-\\\H|g' \
            -e '/<ssh>/{n;s|<enabled>false</enabled>|<enabled>true</enabled>|}' \
            ${NCS_CONFIG_DIR}/ncs.conf
sed -i.bak -e "s|<dir>${NCS_DIR}/etc/ncs/snmp</dir>|<1-- dir>${NCS_DIR}/etc/ncs/snmp</dir -->|"\
           -e '/<ssl>/{n;s|<enabled>false</enabled>|<enabled>true</enabled>|}' \
           ${NCS_CONFIG_DIR}/ncs.conf

printf "${GREEN}##### Reload ncs.conf to enable the NSO northbound interfaces\n${NC}"
ncs_cmd -d -c "reload"

printf "${GREEN}##### NSO setup done. Set up the NSO CLI and RESTCONF based demos\n${NC}"

# NSO CLI and NETCONF client SSH public key authentication for the admin user.
# To, for example, access the NSO CLI over SSH:
# ssh -i /${NSOAPP_NAME}/mgr_admin_ed25519 -l admin -p 2024 -o LogLevel=ERROR -o UserKnownHostsFile=/${NSOAPP_NAME}/known_hosts "${NSO_NAME}"
ssh-keygen -N "" -t ed25519 -f /${NSOAPP_NAME}/mgr_admin_ed25519
chmod 600 /${NSOAPP_NAME}/mgr_admin_ed25519.pub
chmod 600 /${NSOAPP_NAME}/mgr_admin_ed25519
mkdir -p /home/admin/.ssh
chmod 750 /home/admin/.ssh
cat /${NSOAPP_NAME}/mgr_admin_ed25519.pub >> /home/admin/.ssh/authorized_keys
chmod 640 /home/admin/.ssh/authorized_keys
chown -Rh admin:ncsadmin /home/admin/.ssh

# SSH client known hosts
NSO_HOST_KEY=$(cat ${NCS_CONFIG_DIR}/ssh/ssh_host_ed25519_key.pub | cut -d ' ' -f1-2)
printf "[${NSO_NAME}]:2024 $NSO_HOST_KEY\n" > /${NSOAPP_NAME}/known_hosts
chmod 600 /${NSOAPP_NAME}/known_hosts

# NSO RESTCONF client token for the admin user authentication
restconf_token=$(openssl rand -base64 32)
echo $restconf_token > /home/admin/restconf_token
chown admin:ncsadmin /home/admin/restconf_token
chmod 640 /home/admin/restconf_token

# Run the showcase NSO CLI and RESTCONF client scripts
printf "\n${PURPLE}###### Run an NSO CLI based demo\n${NC}"
/${NSOAPP_NAME}/showcase.sh 3
printf "\n${PURPLE}###### Run an NSO RESTCONF based demo\n${NC}"
python3 -u /${NSOAPP_NAME}/showcase_rc.py 3 $restconf_token
